/**
 * 工具类
 */
var menus = [ {
	"url" : "/new/rule.html",
	"name" : "日志告警"
}, {
	"url" : "/new/times.html",
	"name" : "耗时告警"
}, {
	"url" : "/new/user.html",
	"name" : "邮件组"
}, {
	"url" : "/new/bugs.html",
	"name" : "线上Bug"
}, {
	"url" : "/new/report.html",
	"name" : "性能监控"
}, {
	"url" : "/new/os.html",
	"name" : "机器监控"
}, {
	"url" : "/new/map.html",
	"name" : "字典"
},{
	"url" : "/new/prof.html",
	"name" : "进程性能"
},  {
	"url" : "/base/call/EmailSummaryTask/report",
	"name" : "Bug汇总"
}, {
	"url" : "/base/call/LogReportTask/report?args[20,1]",
	"name" : "昨日调用"
} ];


$.ajaxSetup({
	error : function(xhr) {
		alert('发生错误,请重试' + xhr);
	}
});

Date.prototype.Format = function(fmt) {
	var o = {
		"M+" : this.getMonth() + 1,
		"d+" : this.getDate(),
		"H+" : this.getHours(),
		"m+" : this.getMinutes(),
		"s+" : this.getSeconds(),
		"q+" : Math.floor((this.getMonth() + 3) / 3),
		"S" : this.getMilliseconds()

	};
	if (/(y+)/.test(fmt))
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	for ( var k in o)
		if (new RegExp("(" + k + ")").test(fmt))
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
					: (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
}

function getTime(eId) {
	return new Date($(eId).datetimebox('getValue')).getTime();
}

function initNavMenu(eId) {
	var html = "";
	var cur = window.location.pathname;
	for ( var k in menus) {
		var menu = menus[k];
		var url = menu.url;
		html += '<li><a href="' + menu.url + '">' + menu.name + '</a></li>';
	}
	$(eId).html(html);
}

var Util = {
	SELECT_DFT : '--请选择--',
	getCombobox : function(id) {
		var val = $(id).combobox('getValue');
		return val == Util.SELECT_DFT ? undefined : val;
	},
	builCombox : function(eid, url, key, val) {
		$(eid).combobox({
			url : url,
			textField : val,
			valueField : key,
			// panelWidth:"auto",
			// panelHeight:"auto",
			value : Util.SELECT_DFT,
			loadFilter : function(data) {
				var ndata = data.slice(0), all = {};
				all[key] = Util.SELECT_DFT;
				all[val] = Util.SELECT_DFT;
				ndata.unshift(all);
				return ndata;
			}
		});
	},
	builCombVal : function(eid, data, key, val) {
		$(eid).combobox({
			data : data,
			textField : val,
			valueField : key,
			value : Util.SELECT_DFT,
			loadFilter : function(data) {
				var ndata = data.slice(0), all = {};
				all[key] = Util.SELECT_DFT;
				all[val] = Util.SELECT_DFT;
				ndata.unshift(all);
				return ndata;
			}
		});
	},
	builCombValSelect : function(eid, data, key, val, select, editable,
			multiple) {
		$(eid).combobox({
			data : data,
			value : select,
			textField : key,
			valueField : val,
			editable : editable,
			multiple : multiple
		});
	},
	getJsonValues : function(json) {
		var values = [];
		for (k in json)
			values.push(json[k]);
		return values;
	},
	callServer : function(beanId, method, jsonParam, callBack) {
		var url = '/base/' + beanId + '/' + method;
		$.get(url, jsonParam, callBack);
	},
}
