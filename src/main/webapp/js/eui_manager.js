/**
 * easyUI管理类
 */
function UIAction(tbl, beanName, url, getFormVal) {

	var that = this;
	var saveStr = '<a href="javascript:void(0)" style="color:red" onclick="action.saverow(this)">Save</a> ';
	var editStr = '<a href="javascript:void(0)" style="color:red" onclick="action.editrow(this)">Edit</a> ';
	var delStr = '<a href="javascript:void(0)"  style="color:red" onclick="action.deleterow(this)">Delete</a>'
	var cacleStr = '<a href="javascript:void(0)" style="color:red" onclick="action.cancelrow(this)">Cancel</a>';
	var saveCancel = saveStr + cacleStr;
	var editDel = editStr + delStr;

	/** 提交到后端* */
	this.request = function(index, action, row, callback) {
		row.class = beanName;
		$.ajax({
			url : url + action,
			data : {
				'params' : JSON.stringify(row)
			},
			success : function(res) {
				if (callback)
					callback(index, row, res);
				else
					tbl.datagrid('refreshRow', index);
			}
		});
	};

	/** 添加数据* */
	this.insert = function() {
		var index = 0;
		tbl.datagrid('insertRow', {
			index : index,
			row : {
				status : 'P',
				adding : true
			}
		});
		tbl.datagrid('selectRow', index);
		tbl.datagrid('beginEdit', index);
	};

	/** 编辑回调* */
	this.onEndEdit = function(index, row) {
		if (row.adding) {
			that.request(index, 'save', row);
			row.adding = false;
		} else {
			that.request(index, 'update', row);
		}
	};

	this.onBeforeEdit = function(index, row) {
		row.editing = true;
		tbl.datagrid('refreshRow', index);
	};
	this.onAfterEdit = function(index, row) {
		row.editing = false;
		tbl.datagrid('refreshRow', index);
	};
	this.onCancelEdit = function(index, row) {
		row.editing = false;
		if (row.adding) {
			// 如果新加时取消,需要删除空白行
			row.adding = false;
			tbl.datagrid('deleteRow', index);
		} else {
			tbl.datagrid('refreshRow', index);
		}
	};

	this.getRowIndex = function(target) {
		var tr = $(target).closest('tr.datagrid-row');
		return parseInt(tr.attr('datagrid-row-index'));
	};

	this.editrow = function(target) {
		tbl.datagrid('beginEdit', this.getRowIndex(target));
	};

	this.deleterow = function(target) {
		$.messager.confirm('Confirm', 'Are you sure?', function(r) {
			if (r) {
				var index = that.getRowIndex(target);
				var row = tbl.datagrid('getSelected');
				that.request(index, 'delete', row, function(index, param, res) {
					tbl.datagrid('deleteRow', index);
				});
			}
		});
	};

	this.saverow = function(target) {
		tbl.datagrid('endEdit', this.getRowIndex(target));
	};

	this.cancelrow = function(target) {
		tbl.datagrid('cancelEdit', this.getRowIndex(target));
	};

	this.onLoadSuccess = function(data) {
		tbl.query_return_total = data.total;
	};

	this.onBeforeLoad = function(data) {
		data.total = tbl.query_return_total;
		if (!that.json) {
			that.json = {
				"class" : beanName
			};
		}
		if (getFormVal) {
			var args = getFormVal();
			if (that.compareCopy(args, that.json))
				data.total = undefined;
		}
		data.params = JSON.stringify(that.json);
	};

	this.compareCopy = function(src, to) {
		var isChange = false;
		for ( var k in src) {
			if (src[k] != to[k]) {
				to[k] = src[k];
				isChange = true;
			}
		}
		return isChange;
	};

	this.buildAction = function(value, row, index) {
		return row.editing ? saveCancel : editDel;
	};

	this.configTable = function(toolBar, columns) {
		var urlInit = url + 'query';

		tbl.datagrid({
			url : urlInit,
			pageSize : 20,
			toolbar : toolBar,
			columns : columns,
			pagination : true,
			fitColumns : true,
			singleSelect : true,
			iconCls : 'icon-edit',
			pageList : [ 20, 30, 50 ],
			onEndEdit : this.onEndEdit,
			onAfterEdit : this.onAfterEdit,
			onBeforeEdit : this.onBeforeEdit,
			onCancelEdit : this.onCancelEdit,
			onLoadSuccess : this.onLoadSuccess,
			onBeforeLoad : this.onBeforeLoad,
		});
		tbl.datagrid('getPager').pagination({
			beforePageText : '第',// 页数文本框前显示的汉字
			afterPageText : '页    共 {pages} 页',
			displayMsg : '当前显示 {from} - {to} 条记录   共 {total} 条记录',
		});
	};
}

/*******************************************************************************
 * 扩展easyui 增加password类型
 */
(function($) {
	var $pwd = $('<input type="password" class="datagrid-editable-input" />');
	$.extend($.fn.datagrid.defaults.editors, {
		password : {
			init : function(container, options) {
				return $pwd.appendTo(container);
			},
			destroy : function(target) {
				$(target).remove();
			},
			getValue : function(target) {
				return $(target).val();
			},
			setValue : function(target, value) {
				$(target).val(value);
			},
			resize : function(target, width) {
				$(target)._outerWidth(width);
			}
		}
	});
})(jQuery);


