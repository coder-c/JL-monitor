[ {
	"render" : "cpu",
	"type" : "spline",
	"x" : [ "cpu", "iowait" ]
}, {
	"render" : "flow",
	"type" : "area",
	"x" : [ "recv", "send" ]
}, {
	"render" : "load",
	"type" : "spline",
	"x" : [ "minute1", "minute5", "minute15" ]
}, {
	"render" : "mem",
	"type" : "area",
	"x" : [ "MemTotal", "MemFree", "Cached", "SwapCached", "SwapTotal" ]
} ]
