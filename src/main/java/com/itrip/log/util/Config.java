package com.itrip.log.util;

import java.util.Properties;

/**
 * Function:工具类
 *
 * @date:2016年7月23日/下午5:38:00
 * @Author:jeff@aoliday.cao
 * @version:1.0
 */
public class Config {

	private static Properties prop = System.getProperties();
	
	public static int getInt(String key, int defaultVal) {
		String property = prop.getProperty(key);
		if (property != null) {
			return Integer.valueOf(property);
		}
		return defaultVal;
	}
	
	public static long getLong(String key, long defaultVal) {
		String property = prop.getProperty(key);
		if (property != null) {
			return Long.valueOf(property);
		}
		return defaultVal;
	}
	
	public static double getDouble(String key, double defaultVal) {
		String property = prop.getProperty(key);
		if (property != null) {
			return Double.valueOf(property);
		}
		return defaultVal;
	}
	
	public static float getFloat(String key, float defaultVal) {
		String property = prop.getProperty(key);
		if (property != null) {
			return Float.valueOf(property);
		}
		return defaultVal;
	}
	
	public static String getString(String key, String defaultVal) {
		String property = System.getProperty(key);
		if (property != null) {
			return property;
		}
		return defaultVal;
	}
	
}
