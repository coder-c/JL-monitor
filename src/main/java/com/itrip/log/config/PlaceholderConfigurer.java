package com.itrip.log.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import com.itrip.log.util.Util;


public class PlaceholderConfigurer extends PropertyPlaceholderConfigurer {

	private List<String> deocdes = new ArrayList<String>();

	public void setDeocdes(List<String> deocdes) {
		this.deocdes = deocdes;
	}

	@Override
	protected String resolvePlaceholder(String placeholder, Properties props) {
		String resolvePlaceholder = super.resolvePlaceholder(placeholder, props);
		for (String item : deocdes) {
			if (item.equals(placeholder)) {
				return Util.decrypt(resolvePlaceholder);
			}
		}
		System.getProperties().putAll(props);
		return resolvePlaceholder;
	}
}
