package com.itrip.log.module.collect;

import com.itrip.log.core.IServer;


/**
 * Function:日志输入模块
 *
 * @date:2016年10月19日/下午6:25:44
 * @Author:coder_czp@126.com
 * @version:1.0
 */
public interface ILogInputStream extends Runnable,IServer{

	/***
	 * 读取日志行
	 * @return
	 */
	void run();
	
	/***
	 * 采集器的名称
	 * @return
	 */
	String name();
	
	/***
	 * 停止输入流
	 */
	void stop();
	
}
