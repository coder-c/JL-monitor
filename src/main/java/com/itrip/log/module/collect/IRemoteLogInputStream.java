package com.itrip.log.module.collect;

import com.itrip.log.module.db.domain.HostBean;

/**
 * Function:远程日志输入流
 *
 * @date:2016年10月19日/下午6:31:59
 * @Author:coder_czp@126.com
 * @version:1.0
 */
public interface IRemoteLogInputStream extends ILogInputStream {

	/***
	 * 获取远程主机
	 * @return
	 */
	HostBean getRemoteHost();
}
