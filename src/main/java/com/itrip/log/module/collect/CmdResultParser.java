package com.itrip.log.module.collect;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.itrip.log.util.Util;

/**
 * Function:解析shell命令结果<br>
 *
 * Date :2016年2月22日 <br>
 * Author :coder_czp@126.com<br>
 * Copyright (c) 2015,coder_czp@126.com All Rights Reserved.
 */
public class CmdResultParser {

	private JSONObject parseSwap(JSONObject json, String swap) {
		String swap1 = swap.substring(swap.indexOf(":") + 1);
		String[] swapar = swap1.split("[,|.]");
		for (int i = 0; i < swapar.length; i++) {
			String item = swapar[i].toLowerCase();
			long num1 = Util.getNum(item).longValue();
			if (item.contains("total")) {
				json.put("stotal", num1 * 1024);
			} else if (item.contains("used")) {
				json.put("sused", num1 * 1024);
			} else if (item.contains("free")) {
				json.put("sfree", num1 * 1024);
			} else if (item.contains("buff")) {
				json.put("scached", num1 * 1024);
			}
		}
		return json;
	}

	private JSONObject parseMem(String mem) {
		String mem1 = mem.substring(mem.indexOf(":") + 1);
		String[] memar = mem1.split(",");
		JSONObject json = new JSONObject();
		for (int i = 0; i < memar.length; i++) {
			String item = memar[i].toLowerCase();
			long num1 = Util.getNum(item).longValue();
			if (item.contains("total")) {
				json.put("total", num1 * 1024);
			} else if (item.contains("used")) {
				json.put("used", num1 * 1024);
			} else if (item.contains("free")) {
				json.put("free", num1 * 1024);
			} else if (item.contains("buff")) {
				json.put("cache", num1 * 1024);
			}
		}
		return json;
	}

	private JSONObject parseCpu(String str) {
		List<Number> nums = Util.getNumber(str);
		JSONObject json = new JSONObject();
		json.put("us", nums.get(0).floatValue());
		json.put("sy", nums.get(1).floatValue());
		json.put("id", nums.get(3).floatValue());
		json.put("wa", nums.get(4).floatValue());
		return json;
	}

	private JSONObject parseLoad(String loadStr) {
		List<Number> nums = Util.getNumber(loadStr);
		JSONObject load = new JSONObject();
		int len = nums.size();
		load.put("m1", 100 * nums.get(len - 3).floatValue());
		load.put("m5", 100 * nums.get(len - 2).floatValue());
		load.put("m15", 100 * nums.get(len - 1).floatValue());
		return load;
	}

	private JSONObject parseNet(String net) {
		JSONObject json = new JSONObject();
		String[] arr = net.split(",");
		for (String string : arr) {
			String[] ar2 = string.split(":");
			json.put(ar2[0], ar2.length < 2 ? 0 : ar2[1]);
		}
		return json;
	}

	private String getLastResult(String result) {
		int lastTop = result.lastIndexOf("top -");
		if (lastTop > -1) {
			result = result.substring(lastTop);
		}
		return result;
	}

	public static String cmd() {
		// 需要连续执行2次才能采集到CPU利用率,解析时丢弃前N次
		StringBuffer netCmd = new StringBuffer();
		netCmd.append("top -bc -n2|egrep 'Tasks|load|Cpu|Mem|Swap' && ");
		netCmd.append("netstat -ant |grep ESTABLISHED|");
		netCmd.append("awk 'BEGIN{split(\"3306,80\",ports,\",\")}");
		netCmd.append("\n{for(i in ports){tmp=ports[i];");
		netCmd.append("if(index($4,tmp)>0){ain[tmp]++};");
		netCmd.append("if(index($5,tmp)>0){aout[tmp]++}}");
		netCmd.append("conns++;}");
		netCmd.append("\nEND{ printf(\"all:%s,\",conns);");
		netCmd.append("for(j in ain)printf(\"in_%s:%s,\",j,ain[j]);");
		netCmd.append("for(k in aout)printf(\"out_%s:%s,\",k,aout[k]); }'");
		return netCmd.toString();
	}

	public JSONObject parseForDB(String result) {
		result = getLastResult(result);
		String[] lines = result.split("\n");
		JSONObject load = parseLoad(lines[0]);
		//JSONObject tasks = parseTasks(lines[1]);
		JSONObject cpu = parseCpu(lines[2]);
		JSONObject mem = parseMem(lines[3]);
		JSONObject swap = parseSwap(mem,lines[4]);
		JSONObject netConn = parseNet(lines[lines.length - 1]);
		JSONObject data = new JSONObject();
		//data.put("tasks", tasks);
		data.put("load", load);
		data.put("cpu", cpu);
		data.put("mem", swap);
		data.put("net", netConn);
		return data;
	}

}
