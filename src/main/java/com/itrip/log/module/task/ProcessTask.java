package com.itrip.log.module.task;
///* 
// * 创建日期 2016-02-19
// *
// * 成都澳乐科技有限公司
// * 电话：028-85253121 
// * 传真：028-85253121
// * 邮编：610041 
// * 地址：成都市武侯区航空路6号丰德国际C3
// * 版权所有
// */
//package com.itrip.log.task;
//
//import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.Set;
//
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.itrip.log.alarm.AlarmManager;
//import com.itrip.log.domain.ProcessAlarm;
//import com.itrip.log.shell.ConnManager;
//import com.itrip.log.util.MailQueue;
//import com.itrip.log.util.Util;
//
///**
// * 
// * author:jeff.cao@aoliday.com date:2016年3月1日
// */
//public class ProcessTask {
//
//	private MailQueue mailQueue;
//	private AlarmManager alarmManager;
//	private String template = "[%s]执行[%s]发现告警";
//
//
//	public void setMailQueue(MailQueue mailQueue) {
//		this.mailQueue = mailQueue;
//	}
//
//	public void setAlarmManager(AlarmManager alarmManager) {
//		this.alarmManager = alarmManager;
//	}
//
//	public void run() {
//		try {
//			ConnManager ins = ConnManager.getIns();
//			JSONArray shellMap = Util.loadJSONArray("classpath:/map/task.js");
//			List<ProcessAlarm> procRules = alarmManager.getProcRules();
//			for (ProcessAlarm proc : procRules) {
//				String cmd = getShellFrom(shellMap, proc.getShell());
//				if (cmd.length() == 0)
//					continue;
//
//				if ("*".equals(proc.getHost())) {
//					Map<String, String> res = ins.executeInAll(cmd);
//					Set<Entry<String, String>> entrySet = res.entrySet();
//					for (Entry<String, String> entry : entrySet) {
//						testHasError(proc, entry.getKey(), cmd, entry.getValue());
//					}
//				} else {
//					String res = ins.executeInHost(proc.getHost(), cmd);
//					testHasError(proc, proc.getHost(), cmd, res);
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private String getShellFrom(JSONArray shellMap, String shell) {
//		for (int i = 0; i < shellMap.size(); i++) {
//			JSONObject json = shellMap.getJSONObject(i);
//			if (json.getString("name").equals(shell))
//				return json.getString("shell");
//		}
//		System.out.println("shell name not found:" + shell);
//		return "";
//	}
//
//	private void testHasError(ProcessAlarm proc, String host, String cmd, String res) {
//		if (res.contains(proc.getReturnKeyword())) {
//			String message = String.format(template, host, cmd);
//			mailQueue.addEmailTask(proc.getEmailGroup(), message);
//		}
//	}
//
//}
