package com.itrip.log.module.task;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.itrip.log.util.VelocityUtil;


/**
 * Function:性能分析报表
 *
 * @date:2016年10月21日/下午3:56:21
 * @Author:jeff@aoliday.cao
 * @version:1.0
 */
@Component("LogReportTask")
public class LogReportTask extends BaseTask {

	private String mailGroup = "dev_all";
	
	private String templatePath = "log_report.vm";

	@Scheduled(cron = "0 15 10 ? * *")
	public void senReport() {
		reportAndSendMail();
	}

	private StringBuffer doQuery() {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("static_type", "dubbo");
		param.put("limit", 0);
		param.put("size", 200);
		param.put("calls", 5);
		param.put("table", getTable(1));

		Map<String, Object> map = new HashMap<String, Object>();
		param.put("group", "method");
		List<Map<String, Object>> results = daoTemplate.selectList("common.selectReport", param);
		map.put("method", processProc(results));
		
		param.put("notSum", "true");
		param.put("group", "proc");
		List<Object> procRes = daoTemplate.selectList("common.selectReport", param);
		map.put("proc", procRes);
		
		param.put("group", "server");
		List<Object> serRes = daoTemplate.selectList("common.selectReport", param);
		map.put("server", serRes);
		map.putAll(param);
		
		String vm = VelocityUtil.getIns().getTemplate(templatePath, map);
		return new StringBuffer(vm);
	}

	private Map<String,TreeSet<Map<String, Object>>> processProc(List<Map<String, Object>> results) {
		Map<String,TreeSet<Map<String, Object>>> map = new HashMap<String, TreeSet<Map<String,Object>>>();
		for (Map<String, Object> item : results) {
			String proc = item.get("proc").toString();
			TreeSet<Map<String, Object>> list = map.get(proc);
			if(list==null){
				list = new TreeSet<Map<String,Object>>(new Comparator<Map<String,Object>>() {

					@Override
					public int compare(Map<String, Object> o1, Map<String, Object> o2) {
						Double d1 = Double.valueOf(o1.get("avgt").toString());
						Double d2 = Double.valueOf(o2.get("avgt").toString());
						return d2.compareTo(d1);
					}
				});
				map.put(proc, list);
			}
			list.add(item);
		}
		return map;
	}

	@SuppressWarnings("deprecation")
	private String getTable(int i) {
		if (i == 0)
			return "static_method_m1";
		Date d = new Date();
		d.setDate(d.getDate() - i);
		SimpleDateFormat sd = new SimpleDateFormat("yyyy_MM_dd");
		return "static_method_m1_" + sd.format(d);
	}

	public StringBuffer report() {
		return doQuery();
	}

	public StringBuffer reportAndSendMail() {
		try {
			StringBuffer res = doQuery();
			mailQueue.addEmailTask(mailGroup, mailGroup, res, mailGroup);
			return res;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new StringBuffer();
	}
}
