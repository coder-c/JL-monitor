/* 
 * 创建日期 2016-02-19
 *
 * 成都澳乐科技有限公司
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 * 版权所有
 */
package com.itrip.log.module.task;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.itrip.log.module.db.domain.EmailRecord;
import com.itrip.log.module.db.server.EmailRecordManager;
import com.itrip.log.util.VelocityUtil;
import com.itrip.log.util.Util;

/**
 * 
 * 每周五下午两点发送汇总报告 <br>
 * author:jeff.cao@aoliday.com date:2016年3月2日
 */
@Component("EmailSummaryTask")
public class EmailSummaryTask extends BaseTask {

	private String mailGroup = "sys_admin";

	@Autowired
	private EmailRecordManager emailMgr;

	// @Scheduled(cron = "0/10 * * * * *")
	@Scheduled(cron = "0 0 14 ? * FRI")
	public void run() {
		try {
			StringBuffer report = report();
			mailQueue.addEmailTask(mailGroup, mailGroup, report, mailGroup);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public StringBuffer report() {
		long end = System.currentTimeMillis();
		long time = Util.getXDayAgoTime(7, true);
		List<EmailRecord> projectData = emailMgr.getProjectData(time, end);
		Collection<EmailRecord> managerData = buildManagerData(projectData);
		List<EmailRecord> personData = emailMgr.getPersonData(time, end);

		SimpleDateFormat fs = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", fs.format(new Date(time)));
		map.put("end", fs.format(new Date(end)));
		map.put("project", projectData);
		map.put("manager", managerData);
		map.put("dev", personData);

		String template = VelocityUtil.getIns().getTemplate("log_record.vm", map);
		return new StringBuffer(template);
	}

	private Collection<EmailRecord> buildManagerData(List<EmailRecord> projectData) {
		Map<String, EmailRecord> map = new HashMap<String, EmailRecord>();
		HashSet<String> projects = new HashSet<String>();
		for (EmailRecord item : projectData) {
			if (item.getKeyword().contains("Full GC"))
				continue;
			String project = item.getProject();
			if (project.contains("sys_admin"))
				continue;
			if (projects.contains(project))
				continue;

			String manager = item.getManager();
			EmailRecord cc = map.get(manager);
			if (cc == null) {
				cc = new EmailRecord();
				cc.setId(item.getId());
				cc.setManager(manager);
				map.put(manager, cc);
			} else {
				cc.setId(item.getId() + cc.getId());
				map.put(manager, cc);
			}
			projects.add(project);
		}
		List<EmailRecord> arrayList = new ArrayList<EmailRecord>(map.values());
		Collections.sort(arrayList, new Comparator<EmailRecord>() {

			@Override
			public int compare(EmailRecord o1, EmailRecord o2) {
				return o2.getId()>o1.getId()?1:o2.getId()==o1.getId()?0:-1;
			}
		});
		return arrayList;
	}

	public StringBuffer reportWithMail() {
		StringBuffer infoProject = report();
		mailQueue.addEmailTask(mailGroup, mailGroup, infoProject, mailGroup);
		return infoProject;
	}

	public void setMailGroup(String mailGroup) {
		this.mailGroup = mailGroup;
	}
}
