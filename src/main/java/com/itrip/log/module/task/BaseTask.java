package com.itrip.log.module.task;

import org.springframework.beans.factory.annotation.Autowired;

import com.itrip.log.module.db.IDao;
import com.itrip.log.util.MailQueue;

//http://www.cnblogs.com/xiaopeng84/archive/2009/11/26/1611427.html
//http://gong1208.iteye.com/blog/1773177
public class BaseTask {

	@Autowired
	protected IDao daoTemplate;

	@Autowired
	protected MailQueue mailQueue;

	public void setDaoTemplate(IDao daoTemplate) {
		this.daoTemplate = daoTemplate;
	}

	public void setMailQueue(MailQueue mailQueue) {
		this.mailQueue = mailQueue;
	}

}
