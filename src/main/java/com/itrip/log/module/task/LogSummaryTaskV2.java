///* 
// * 创建日期 2016-02-19
// *
// * 成都澳乐科技有限公司
// * 电话：028-85253121 
// * 传真：028-85253121
// * 邮编：610041 
// * 地址：成都市武侯区航空路6号丰德国际C3
// * 版权所有
// */
//package com.itrip.log.module.task;
//
//import java.sql.Statement;
//import java.util.Calendar;
//import java.util.concurrent.BlockingQueue;
//import java.util.concurrent.LinkedBlockingQueue;
//
//import org.apache.ibatis.session.SqlSession;
//import org.mybatis.spring.SqlSessionTemplate;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import com.itrip.log.core.ShutdownCallback;
//
///**
// * 数据汇总线程 <br>
// * author:jeff.cao@aoliday.com date:2016年3月7日
// */
//@Component("LogSummaryTaskV2")
//public class LogSummaryTaskV2 extends BaseTask implements Runnable,ShutdownCallback {
//
//	public static String sumSql = "INSERT INTO static_method_m1(static_type,server,proc,api,method,calls,min_time_cost,max_time_cost,avg_time_cost,t1,t2,t3,t4,t5,create_time)";
//	private static Logger logger = LoggerFactory.getLogger(LogSummaryTaskV2.class);
//	private BlockingQueue<String> records = new LinkedBlockingQueue<String>(2000);
//	private volatile boolean hasStart = false;
//	
//	@Scheduled(cron = "0 0/1 * * * *")
//	public void saveLog() {
//		try {
//			startSaveThread();
//			checkIsNecessaryCreateNewTable();
//			for (ICount iCount : handlers) {
//				String sql = iCount.getCountSql();
//				if (sql.length() > 0)
//					records.add(sql);
//			}
//		} catch (Exception e) {
//			logger.error("error:", e);
//		}
//	}
//
//	private void startSaveThread() {
//		if (!hasStart) {
//			synchronized (this) {
//				Thread worker = new Thread(this);
//				worker.setName("StaticResult-woker:" + worker.getId());
//				worker.setDaemon(true);
//				worker.start();
//				hasStart = true;
//			}
//		}
//	}
//
//	private void checkIsNecessaryCreateNewTable() {
//		Calendar now = Calendar.getInstance();
//		if (now.get(Calendar.HOUR_OF_DAY) == 0 && now.get(Calendar.MINUTE) == 0) {
//			String sql = doParation("static_method_m1", now);
//			records.add(sql);
//		}
//	}
//
//	/**
//	 * 
//	 * <pre>
//	 * alter table xxx rename xxx_year_month_date
//	 * create table if not exists xxx like xxx_year_month_date
//	 * INSERT INTO static_url_m1 (...) values(...)
//	 * </pre>
//	 * 
//	 * @param table
//	 * @param insert
//	 * 
//	 * @param now
//	 * @return
//	 */
//	private String doParation(String table,Calendar now) {
//		int year = now.get(Calendar.YEAR);
//		int date = now.get(Calendar.DATE);
//		int month = now.get(Calendar.MONTH) + 1;
//		String newName = String.format("%s_%s_%02d_%02d", table, year, month, date-1);
//		String rename = String.format("alter table %s rename %s", table, newName);
//		String create = String.format("create table if not exists %s like %s", table, newName);
//		return String.format("%s;%s;", rename, create);
//	}
//
//	@Override
//	public void run() {
//		try {
//			String take = null;
//			SqlSessionTemplate template = daoTemplate.getSqlSessionTemplate();
//			SqlSession session = template.getSqlSessionFactory().openSession();
//			Statement st = session.getConnection().createStatement();
//			while (!Thread.interrupted()) {
//				try {
//					take = records.take();
//					st.executeUpdate(take);
//				} catch (Exception e) {
//					logger.error("fail to save {}", take, e);
//				}
//			}
//			st.close();
//			session.close();
//		} catch (Exception e) {
//			logger.error("fail to save log", e);
//		}
//	}
//
//	@Override
//	public void onSystemExit() {
//		int size = records.size();
//		if(size==0)
//			return;
//		
//		try {
//			SqlSessionTemplate template = daoTemplate.getSqlSessionTemplate();
//			SqlSession session = template.getSqlSessionFactory().openSession();
//			Statement st = session.getConnection().createStatement();
//			for (int i = 0; i < size; i++) {
//				st.executeUpdate(records.take());
//			}
//			st.close();
//			session.close();
//		} catch (Exception e) {
//			logger.error("fail to save log", e);
//		}
//	}
//}
