package com.itrip.log.module.handler;

import com.itrip.log.core.IServer;
import com.itrip.log.core.ReaderBean;


/**
 * Function:XXX<br>
 *
 * Date :2016年2月22日 <br>
 * Author :coder_czp@126.com<br>
 * Copyright (c) 2015,coder_czp@126.com All Rights Reserved.
 */
public interface ILogHandler extends  IServer {

	public void handle(ReaderBean bean);
	
	public boolean match(String file);
}
