package com.itrip.log.module.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itrip.log.module.db.domain.SpecLog;

/**
 * Function:保存groovy执行记录
 *
 * @date:2016年10月14日/上午9:44:44
 * @Author:jeff@aoliday.cao
 * @version:1.0
 */
public class GroovyLogSaveHandler extends MutilLineLogHandler {

	private Logger logger = LoggerFactory.getLogger(GroovyLogSaveHandler.class);

	@Override
	public boolean match(String file) {
		return file.endsWith("request.log");
	}

	@Override
	public String getLineConcater() {
		return "<br/>";
	}

	@Override
	public void processLines(String host, String file, String proc, StringBuffer lines) {
		String key = "execute";
		if (lines.indexOf("ManagerServlet") > -1 && lines.indexOf(key) > -1) {
			int exeIndex = lines.indexOf(key);
			int infoIndex = lines.lastIndexOf("<br/>");
			if (infoIndex > -1) {
				String userId = "catn't get";
				int userIndex = lines.indexOf("user");
				if (userIndex > -1) {
					userId = lines.substring(userIndex + 4, exeIndex).trim();
				}
				SpecLog log = new SpecLog();
				log.setLog(lines.substring(exeIndex + key.length(), infoIndex));
				log.setCreateTime(System.currentTimeMillis());
				log.setUserId(userId);
				log.setProject(proc);
				log.setHost(host);
				dao.save("SpecLog", log);
			} else {
				logger.error("error groovy format:{}", lines);
			}
		}
	}

}
