package com.itrip.log.module.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itrip.log.core.AbstractModule;
import com.itrip.log.core.ReaderBean;

/**
 * Function:日志分析模块
 *
 * @date:2016年10月19日/下午7:53:38
 * @Author:coder_czp@126.com
 * @version:1.0
 */
public class HandlerModule extends AbstractModule<ILogHandler> {

	private static Logger log = LoggerFactory.getLogger(HandlerModule.class);

	@Override
	public String name() {
		return "LogHandlerModule";
	}

	public void handle(ReaderBean bean) {
		for (ILogHandler handler : servers) {
			if (handler.match(bean.getFile())) {
				handler.handle(bean);
				log.debug("process:{} handler:{}", bean, handler);
			}
		}
	}
}
