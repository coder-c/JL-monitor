/* 
 * 创建日期 2016-02-19
 *
 * 成都澳乐科技有限公司
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 * 版权所有
 */
package com.itrip.log.module.handler;

import com.itrip.log.core.BootstrapServer;
import com.itrip.log.module.db.AnsySQLExecutor;
import com.itrip.log.module.db.IDao;
import com.itrip.log.module.db.server.AlarmManager;
import com.itrip.log.util.MailQueue;

/**
 * 
 * author:jeff.cao@aoliday.com date:2016年2月23日
 */
public abstract class AbstractHandler implements ILogHandler {

	protected AnsySQLExecutor ansySQLExecutor;
	
	protected AlarmManager alarmManager;
	
	protected MailQueue mQueue;
	
	protected IDao dao;
	
	public IDao getDao() {
		return dao;
	}

	public void setDao(IDao dao) {
		this.dao = dao;
	}

	public void setmQueue(MailQueue mQueue) {
		this.mQueue = mQueue;
	}

	public void setAlarmManager(AlarmManager alarmManager) {
		this.alarmManager = alarmManager;
	}

	public AnsySQLExecutor getAnsySQLExecutor() {
		return ansySQLExecutor;
	}

	public void setAnsySQLExecutor(AnsySQLExecutor ansySQLExecutor) {
		this.ansySQLExecutor = ansySQLExecutor;
	}

	@Override
	public void setBoostServer(BootstrapServer server) {
		
	}

	@Override
	public void stop() {
		
	}

	@Override
	public void start() {
		
	}
	
}
