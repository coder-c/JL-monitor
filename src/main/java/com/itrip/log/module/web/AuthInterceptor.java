///* 
// * 创建日期 2016-11-10
// *
// * 成都澳乐科技有限公司版权所有
// * 电话：028-85253121 
// * 传真：028-85253121
// * 邮编：610041 
// * 地址：成都市武侯区航空路6号丰德国际C3
// */
//package com.itrip.log.module.web;
//
//import org.springframework.stereotype.Component;
//
///**
// * Function:XXX TODO add desc
// *
// * @date:2016年11月30日/下午1:24:55
// * @Author:jeff.cao@aoliday.com
// * @version:1.0
// */
//@Aspect
//@Component
//public class AuthInterceptor {
//
//	//@Around("execution(* com.itrip.pms.controller..*.*(..))")
//	public Object around(ProceedingJoinPoint pjp) throws Throwable {
//		return pjp.proceed();
//	}
//
//	@Before("execution(* com.itrip.pms.controller..*.*(..)) && !execution(* login(..))")
//	public void before(JoinPoint pjp) throws Throwable {
//		if (WebUtil.getCurrentUser() == null)
//			throw new PMSException(ItripResult.NOT_LOGIN, "pms.error.notlogin");
//	}
//	
//}
//
