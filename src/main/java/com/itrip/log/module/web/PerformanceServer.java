package com.itrip.log.module.web;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.itrip.log.module.db.IDao;

/**
 * Function:性能管理服务
 *
 * @date:2016年12月1日/下午7:50:52
 * @Author:jeff.cao@aoliday.com
 * @version:1.0
 */
@Controller
@RequestMapping("/perf")
public class PerformanceServer extends BaseController {

	@Autowired
	@Qualifier("daoTemplate")
	private IDao dao;

	/***
	 * 查询某个进程的性能
	 * 
	 * @param proc
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/query")
	public JSONObject queryPerformance(String proc, long begin, long end) {
		Map<String, Object> pa = new HashMap<String, Object>();
		pa.put("begin", begin);
		pa.put("proc", proc);
		pa.put("end", end);

		JSONObject json = new JSONObject();
		json.put("gc", doQuery(pa, "gc", "common.gcInfo", "time", "cc"));
		json.put("bugs", doQuery(pa, "Bug", "common.bugInfo", "send_time", "cc"));
		return json;
	}

	private JSONObject doQuery(Map<String, Object> pa, String lineName, String sqlId, String keyName, String valName) {
		JSONArray lines = new JSONArray();
		JSONObject json = new JSONObject();
		json.put("sers", lines);

		JSONObject line = new JSONObject();
		line.put("name", lineName);
		line.put("data", dao.loadKeyValueArray(sqlId, pa, keyName, valName));

		lines.add(line);
		return json;
	}
}
