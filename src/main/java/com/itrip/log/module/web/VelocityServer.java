/* 
 * 创建日期 2016-11-10
 *
 * 成都澳乐科技有限公司版权所有
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 */
package com.itrip.log.module.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Function:Velocity 服务
 *
 * @date:2016年11月30日/下午3:43:34
 * @Author:jeff.cao@aoliday.com
 * @version:1.0
 */
@Controller
public class VelocityServer extends BaseController {

	@RequestMapping(value = "/perf")
	public ModelAndView userStaticLogin(HttpServletRequest request, HttpServletResponse resp) throws Exception {
		request.setAttribute("message", "test");
		return new ModelAndView("log_test");
	}
}
