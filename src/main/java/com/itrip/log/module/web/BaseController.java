package com.itrip.log.module.web;

import com.alibaba.fastjson.JSONObject;

/**
 * Function:基础的web接口
 *
 * @date:2016年6月30日/下午1:44:47
 * @Author:jeff@aoliday.cao
 * @version:1.0
 */
public class BaseController {

	public JSONObject returnJSON(Object data, int code, String message) {
		JSONObject json = new JSONObject();
		json.put("code", code);
		json.put("info", message);
		json.put("data", data);
		return json;
	}

	public JSONObject returnJSON(Object data) {
		JSONObject json = new JSONObject();
		json.put("code", 200);
		json.put("info", "");
		json.put("data", data);
		return json;
	}
}
