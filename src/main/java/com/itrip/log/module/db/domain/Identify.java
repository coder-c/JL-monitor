package com.itrip.log.module.db.domain;

import java.io.Serializable;

/**
 * Function:ID接口
 *
 * @date:2016年8月30日/下午6:19:54
 * @Author:jeff@aoliday.cao
 * @version:1.0
 */
public interface Identify extends Serializable{

	int getId();
}
