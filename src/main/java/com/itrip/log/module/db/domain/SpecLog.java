package com.itrip.log.module.db.domain;


/**
 * Function:特殊日子记录
 *
 * @date:2016年8月4日/下午2:17:57
 * @Author:jeff@aoliday.cao
 * @version:1.0
 */
public class SpecLog {

	private int id;
	private String log;
	private String host;
	private String userId;
	private String project;
	private long createTime;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

}
