package com.itrip.log.module.db;

import static org.iq80.leveldb.impl.Iq80DBFactory.factory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.iq80.leveldb.DB;
import org.iq80.leveldb.Options;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.itrip.log.core.BootstrapServer;
import com.itrip.log.core.IServer;
import com.itrip.log.core.ShutdownCallback;

/**
 * Function:存储日期详情
 *
 * @date:2016年6月26日/下午8:44:52
 * @Author:coder_czp@126.com
 * @version:1.0
 */
public class KVDB implements ShutdownCallback ,IServer{

	private DB db;
	private String dbPath;
	private Options options = new Options();
	private Charset charset = Charset.forName("utf-8");
	private static Logger log = LoggerFactory.getLogger(KVDB.class);
	private static final List<String> EMPTY = new ArrayList<String>();

	public void setDbPath(String dbPath) {
		this.dbPath = dbPath;
	}

	public void init() throws IOException {
		options.createIfMissing(true);
		db = factory.open(new File(dbPath), options);
		log.info("kvdb is inited,db:{} path:{}", db, dbPath);
	}

	public boolean append(String key, String values) {
		log.debug("start append key:{},value:{}", key, values);
		String valuesbs = get(key);
		if (valuesbs == null) {
			log.debug("append not found will add key:{}", key);
			put(key, values);
		} else {
			String newVal = values.concat(",").concat(valuesbs);
			db.put(key.getBytes(charset), newVal.getBytes(charset));
		}
		log.debug("success append key:{},value:{}", key, values);
		return true;
	}

	public boolean put(String key, String values) {
		log.debug("start put to kvdb,key:{},value:{}", key, values);
		db.put(key.getBytes(charset), values.getBytes(charset));
		log.debug("success put to kvdb,key:{},value:{}", key, values);
		return true;
	}

	public String get(String key) {
		log.debug("start get from to kvdb,key:{}", key);
		byte[] values = db.get(key.getBytes(charset));
		if (values == null)
			return null;
		String value = new String(values, charset);
		log.debug("success get from to kvdb,key:{},val:", key);
		return value;
	}

	public String hget(String key, String haskKey) {
		String values = get(key);
		if (values == null)
			return null;
		JSONObject value = JSONObject.parseObject(values);
		return value.getString(haskKey);
	}

	public void hincr(String key, String haskKey) {
		String values = get(key);
		if (values == null) {
			JSONObject json = new JSONObject();
			json.put(haskKey, 1);
			put(key, json.toJSONString());
		} else {
			JSONObject value = JSONObject.parseObject(values);
			value.put(haskKey, value.getIntValue(haskKey) + 1);
			put(key, value.toJSONString());
		}
	}

	public JSONObject hgetAll(String key) {
		String values = get(key);
		if (values == null) {
			return new JSONObject();
		}
		return JSONObject.parseObject(values);
	}

	public boolean hput(String key, String hashKey, Object obj) {
		String values = get(key);
		if (values == null) {
			JSONObject json = new JSONObject();
			json.put(hashKey, obj);
			json.put(key, json.toJSONString());
		} else {
			JSONObject value = JSONObject.parseObject(values);
			value.put(hashKey, obj);
			value.put(key, value.toJSONString());
		}
		return true;
	}

	public List<String> getList(String key) {
		String value = get(key);
		if (value == null)
			return EMPTY;
		String[] arr = value.split(",");
		List<String> res = new ArrayList<String>(arr.length);
		for (String string : arr) {
			res.add(string);
		}
		log.debug("success get from to kvdb,key:{},val:{}", key, res);
		return res;
	}

	@Override
	public void onSystemExit() {
		try {
			if (db != null)
				db.close();
			log.info("kbdb is closed,{}", db);
		} catch (IOException e) {
			log.error("close db error", e);
		}
	}

	@Override
	public void setBoostServer(BootstrapServer server) {
		
	}

	@Override
	public void stop() {
		
	}

	@Override
	public void start() {
		
	}

}
