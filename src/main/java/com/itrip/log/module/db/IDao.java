package com.itrip.log.module.db;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.itrip.log.core.IServer;

/**
 * Function:抽象dao接口
 *
 * @date:2016年4月14日/下午2:55:56
 * @Author:jeff@aoliday.cao
 * @version:1.0
 */
public interface IDao extends IServer {

	public abstract String processClassName();

	public abstract int delete(String namespace, long id);
	
	public abstract int update(String namespace,Object obj);
	
	public abstract SqlSessionTemplate getSqlSessionTemplate();
	
	public abstract Object get(String namespace, Integer id);

	public abstract Object save(String namespace, Object obj);

	public abstract <T> List<T> getListByMap(String namespace, Map<String, Object> param);

	public abstract List<Object> selectKeyValues(String sqlId, Map<String, Object> pa);

	public abstract <T> List<T> selectList(String sqlId, Map<String, Object> pa);

	public abstract void clearCache(long id,String namespace);

	public abstract Integer count(String simpleName, Map<String, Object> pa);

	public abstract JSONObject loadKeyValue(String sqlId, Map<String, Object> param, String keyName, String valName);
	
	public abstract JSONArray loadKeyValueArray(String sqlId, Map<String, Object> param, String keyName, String valName);

}
