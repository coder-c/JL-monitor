package com.itrip.log.module.db.server;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.itrip.log.module.db.DaoTemplate;
import com.itrip.log.module.db.domain.Dictionaries;
import com.itrip.log.module.db.domain.HostBean;

/**
 * Function:配置信息管理类
 *
 * @date:2016年4月28日/下午7:55:39
 * @Author:jeff@aoliday.cao
 * @version:1.0
 */
public class ConfigManager extends DaoTemplate {

	@Override
	public String processClassName() {
		return Dictionaries.class.getName();
	}

	@Override
	public int delete(String namespace, long id) {
		return super.delete(namespace, id);
	}

	public synchronized List<Dictionaries> loadMonitorFiles(HostBean host) {
		List<Dictionaries> systemCfg = selectList("getGlobMonitorFile", null);
		if (host != null) {
			Map<String, Object> cdt = new HashMap<String, Object>();
			cdt.put("host", host.getHost());
			List<Dictionaries> hostCfg = selectList("getHostMonitorFile", cdt);
			systemCfg.addAll(hostCfg);
		}
		return systemCfg;
	}

	public JSONObject loadConfig(String type) {
		HashMap<String, Object> where = new HashMap<String, Object>();
		where.put("type", type);
		return loadKeyValue("common.queryConfig", where, "key", "value");
	}
}
