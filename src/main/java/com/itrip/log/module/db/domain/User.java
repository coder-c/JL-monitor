/* 
 * 创建日期 2016-11-10
 *
 * 成都澳乐科技有限公司版权所有
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 */
package com.itrip.log.module.db.domain;

import java.util.Date;

/**
 * Function:用户bean
 *
 * @date:2016年12月13日/下午1:30:18
 * @Author:jeff.cao@aoliday.com
 * @version:1.0
 */
public class User implements Identify {

	private static final long serialVersionUID = 1L;

	private int id;

	private String name;

	private String email;

	private String password;

	private Date time;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int getId() {
		return id;
	}

}
