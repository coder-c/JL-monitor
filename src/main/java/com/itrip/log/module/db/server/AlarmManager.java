package com.itrip.log.module.db.server;

import java.util.ArrayList;
import java.util.List;

import com.itrip.log.module.db.DaoTemplate;
import com.itrip.log.module.db.domain.KeyWordAlarm;
import com.itrip.log.module.db.domain.OSAlarmBean;
import com.itrip.log.module.db.domain.ProcessAlarm;

/**
 * Function:告警管理<br>
 *
 * Date :2016年2月21日 <br>
 * Author :coder_czp@126.com<br>
 * Copyright (c) 2015,coder_czp@126.com All Rights Reserved.
 */
public class AlarmManager extends DaoTemplate {

	public synchronized List<OSAlarmBean> getOsRules() {
		return new ArrayList<OSAlarmBean>();
	}

	public synchronized List<ProcessAlarm> getProcRules() {
		return getListByMap("ProcessAlarm", null);
	}

	public synchronized List<KeyWordAlarm> getKwRules() {
		return selectList("getKeyWordRules", null);
	}

	public synchronized List<KeyWordAlarm> getGCRules() {
		return selectList("getGCRules", null);
	}

	@Override
	public String processClassName() {
		String name = KeyWordAlarm.class.getName();
		String name1 = OSAlarmBean.class.getName();
		String name2 = ProcessAlarm.class.getName();
		return String.format("%s,%s,%s", name, name1, name2);
	}

}
