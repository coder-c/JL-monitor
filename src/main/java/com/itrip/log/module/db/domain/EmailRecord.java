/* 
 * 创建日期 2016-02-27
 *
 * 成都澳乐科技有限公司
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 * 版权所有
 */
package com.itrip.log.module.db.domain;

import java.io.Serializable;

/**
 * 邮件发送记录
 * 
 * @author 作者
 * @version 1.0
 */
public class EmailRecord implements Serializable {

	private static final long serialVersionUID = 1L;

	private long sendTime = System.currentTimeMillis();
	
	private Integer id;

	private String project;

	private String name;

	private String manager;

	private String keyword;
	
	private String kvid;

	private int pv = 1;

	private float bugsrate;
	

	public String getKvid() {
		return kvid;
	}

	public void setKvid(String kvid) {
		this.kvid = kvid;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getPv() {
		return pv;
	}

	public void setPv(int pv) {
		this.pv = pv;
	}

	public float getBugsrate() {
		return bugsrate;
	}

	public void setBugsrate(float bugsrate) {
		this.bugsrate = bugsrate;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	/** 取得 id */
	public java.lang.Integer getId() {
		return id;
	}

	/** 设置 id */
	public void setId(java.lang.Integer id) {
		this.id = id;
	}

	public java.lang.String getProject() {
		return project;
	}

	public void setProject(java.lang.String project) {
		this.project = project;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSendTime() {
		return sendTime;
	}

	public void setSendTime(long sendTime) {
		this.sendTime = sendTime;
	}

}
