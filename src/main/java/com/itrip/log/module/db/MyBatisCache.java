package com.itrip.log.module.db;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.ibatis.cache.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Function:mybatis cache
 *
 * @date:2016年10月15日/上午10:49:48
 * @Author:coder_czp@126.com
 * @version:1.0
 */
public class MyBatisCache implements Cache {

	private static final ConcurrentHashMap<Object, Object> cache = new ConcurrentHashMap<Object, Object>();
	private Logger log = LoggerFactory.getLogger(MyBatisCache.class);
	private ReadWriteLock lock = new ReentrantReadWriteLock();
	private String id;

	public MyBatisCache() {
		cache.clear();
	}
	public MyBatisCache(String id) {
		this.id = id;
	}

	@Override
	public void clear() {
		cache.clear();
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public Object getObject(Object key) {
		log.debug("hit:{}",key);
		return cache.get(key);
	}

	@Override
	public ReadWriteLock getReadWriteLock() {
		return lock;
	}

	@Override
	public synchronized int getSize() {
		return cache.size();
	}

	@Override
	public void putObject(Object key, Object value) {
		cache.put(key, value);
	}

	@Override
	public Object removeObject(Object key) {
		return cache.remove(key);
	}

}
