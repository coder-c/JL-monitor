/* 
 * 创建日期 2016-02-19
 *
 * 成都澳乐科技有限公司
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 * 版权所有
 */
package com.itrip.log.module.db.server;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.itrip.log.module.db.DaoTemplate;
import com.itrip.log.module.db.domain.EmailGroup;

/**
 * 
 * author:jeff.cao@aoliday.com date:2016年2月26日
 */
public class UserManager extends DaoTemplate {

	public Set<EmailGroup> getEmail(String group) {
		Set<EmailGroup> set = new HashSet<EmailGroup>();
		for (EmailGroup it : getEmailGroup()) {
			if (it.getGroupName().equals(group)) {
				set.add(it);
			}
		}
		return set;
	}

	private List<EmailGroup> getEmailGroup() {
		return getListByMap("EmailGroup", null);
	}

	public String getName(String email) {
		for (EmailGroup it : getEmailGroup()) {
			if (it.getEmail().equals(email)) {
				return it.getName();
			}
		}
		return email;
	}

	@Override
	public String processClassName() {
		return EmailGroup.class.getName();
	}

	public String getManager(String emalGroup) {
		for (EmailGroup it : getEmailGroup()) {
			if (it.getGroupName().equals(emalGroup)) {
				return it.getManager();
			}
		}
		return "not found";
	}
}
