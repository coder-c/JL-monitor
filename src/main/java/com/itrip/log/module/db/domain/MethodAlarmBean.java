/* 
 * 创建日期 2016-02-19
 *
 * 成都澳乐科技有限公司
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 * 版权所有
 */
package com.itrip.log.module.db.domain;

/**
 * 
 * author:jeff.cao@aoliday.com date:2016年2月25日
 */
public class MethodAlarmBean{

	private int id;
	private int period;
	private String proc;
	private String type;
	private String method;
	private String service;
	private int timeThreshold;
	private int requestCount;
	private String emailGroup;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method.trim();
	}

	public String getEmailGroup() {
		return emailGroup;
	}

	public void setEmailGroup(String emailGroup) {
		this.emailGroup = emailGroup;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service.trim();
	}

	public int getTimeThreshold() {
		return timeThreshold;
	}

	public void setTimeThreshold(int timeThreshold) {
		this.timeThreshold = timeThreshold;
	}

	public int getRequestCount() {
		return requestCount;
	}

	public void setRequestCount(int requestCount) {
		this.requestCount = requestCount;
	}

	public String getProc() {
		return proc;
	}

	public void setProc(String proc) {
		this.proc = proc.trim();
	}

}
