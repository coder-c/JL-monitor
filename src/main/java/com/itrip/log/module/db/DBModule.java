package com.itrip.log.module.db;

import com.itrip.log.core.AbstractModule;
import com.itrip.log.core.IServer;

/**
 * Function:DB模块,service都在spring-dao里配置
 *
 * @date:2016年10月20日/下午2:45:15
 * @Author:coder_czp@126.com
 * @version:1.0
 */
public class DBModule extends AbstractModule<IServer> {

	@Override
	public String name() {
		return "DBModule";
	}
	
}
