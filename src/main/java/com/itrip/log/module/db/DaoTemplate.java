/* 
 * 创建日期 2016-02-19
 *
 * 成都澳乐科技有限公司
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 * 版权所有
 */
package com.itrip.log.module.db;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.itrip.log.core.BootstrapServer;

/**
 * 
 * author:jeff.cao@aoliday.com date:2016年2月27日
 * 
 * @param <T>
 */
public class DaoTemplate implements IDao {

	protected BootstrapServer rootServer;
	protected SqlSessionTemplate sqlSessionTemplate;
	private static Logger logger = LoggerFactory.getLogger(DaoTemplate.class);

	public SqlSessionTemplate getSqlSessionTemplate() {
		return sqlSessionTemplate;
	}

	public void setSqlSessionTemplate(SqlSessionTemplate template) {
		this.sqlSessionTemplate = template;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.itrip.log.dao.DaoItf#delete(java.lang.String, int)
	 */
	@Override
	public int delete(String namespace, long id) {
		int delete = sqlSessionTemplate.delete(namespace + ".delete", id);
		return delete;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.itrip.log.dao.DaoItf#get(java.lang.String, java.lang.Integer)
	 */
	@Override
	public Object get(String namespace, Integer id) {
		return sqlSessionTemplate.selectOne(namespace + ".get", id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.itrip.log.dao.DaoItf#save(java.lang.String, java.lang.Object)
	 */
	@Override
	public Object save(String namespace, Object obj) {
		sqlSessionTemplate.insert(namespace + ".create", obj);
		return obj;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.itrip.log.dao.DaoItf#getListByMap(java.lang.String,
	 * java.util.Map)
	 */
	@Override
	public <T> List<T> getListByMap(String namespace, Map<String, Object> param) {
		return sqlSessionTemplate.<T> selectList(namespace.concat(".getListByMap"), param);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.itrip.log.dao.DaoItf#selectKeyValues(java.lang.String,
	 * java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> List<T> selectList(String sqlId, Map<String, Object> pa) {
		return (List<T>) selectKeyValues(sqlId, pa);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.itrip.log.dao.DaoItf#selectKeyValues(java.lang.String,
	 * java.util.Map)
	 */
	@Override
	public List<Object> selectKeyValues(String sqlId, Map<String, Object> pa) {
		return sqlSessionTemplate.selectList(sqlId, pa);
	}

	@Override
	public String processClassName() {
		return null;
	}

	@Override
	public void clearCache(long id, String namespace) {
		logger.info("{} change, id:{}", namespace, id);
	}

	@Override
	public int update(String namespace, Object obj) {
		return sqlSessionTemplate.update(namespace + ".update", obj);
	}

	@Override
	public Integer count(String namespace, Map<String, Object> pa) {
		return sqlSessionTemplate.selectOne(namespace + ".count", pa);
	}

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject loadKeyValue(String sqlId, Map<String, Object> where, String keyName, String valName) {
		JSONObject result = new JSONObject();
		for (Object object : selectKeyValues(sqlId, where)) {
			Map<String, Object> cfg = (Map<String, Object>) object;
			result.put(cfg.get(keyName).toString(), cfg.get(valName));
		}
		return result;
	}

	@Override
	@SuppressWarnings("unchecked")
	public JSONArray loadKeyValueArray(String sqlId, Map<String, Object> param, String keyName, String valName) {
		JSONArray json = new JSONArray();
		for (Object object : selectKeyValues(sqlId, param)) {
			Map<String, Object> cfg = (Map<String, Object>) object;
			JSONArray item = new JSONArray();
			item.add(cfg.get(keyName));
			item.add(cfg.get(valName));
			json.add(item);
		}
		return json;
	}

	public <T> T getOne(String string, Map<String, Object> param) {
		return sqlSessionTemplate.selectOne(string, param);
	}

	public List<Object> list(String string, Map<String, Object> param) {
		return sqlSessionTemplate.selectList(string, param);
		
	}

	@Override
	public void setBoostServer(BootstrapServer server) {
		this.rootServer = server;
	}

	@Override
	public void stop() {
		
	}

	@Override
	public void start() {
		
	}
}
