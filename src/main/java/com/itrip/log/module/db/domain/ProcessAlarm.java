/* 
 * 创建日期 2016-02-19
 *
 * 成都澳乐科技有限公司
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 * 版权所有
 */
package com.itrip.log.module.db.domain;

/**
 * 
 * author:jeff.cao@aoliday.com date:2016年3月1日
 */
public class ProcessAlarm {

	private int id;
	private String shell;
	private String host="*";
	private String emailGroup;
	private String returnKeyword;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getHost() {
		return host;
	}

	public String getEmailGroup() {
		return emailGroup;
	}
	public void setEmailGroup(String emailGroup) {
		this.emailGroup = emailGroup;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getShell() {
		return shell;
	}
	public void setShell(String shell) {
		this.shell = shell;
	}
	public String getReturnKeyword() {
		return returnKeyword;
	}
	public void setReturnKeyword(String returnKeyword) {
		this.returnKeyword = returnKeyword;
	}

}
