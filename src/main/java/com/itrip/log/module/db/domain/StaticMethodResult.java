/* 
 * 创建日期 2016-02-19
 *
 * 成都澳乐科技有限公司
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 * 版权所有
 */
package com.itrip.log.module.db.domain;

/**
 * 
 * author:jeff.cao@aoliday.com date:2016年2月29日
 */
public class StaticMethodResult {

	private int id;
	private String api;
	private String proc;
	private String server;
	private String method;
	private String staticType;
	private float timeCost;
	private long createTime;

	public String getProc() {
		return proc;
	}

	public void setProc(String proc) {
		this.proc = proc;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApi() {
		return api;
	}

	public String getStaticType() {
		return staticType;
	}

	public void setStaticType(String staticType) {
		this.staticType = staticType;
	}

	public void setApi(String api) {
		this.api = api;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public float getTimeCost() {
		return timeCost;
	}

	public void setTimeCost(float timeCost) {
		this.timeCost = timeCost;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return new StringBuffer(server).append(',').append(proc).append(',').append(api).append(',').append(method)
				.toString();
	}

}
