package com.itrip.log.module.db.domain;


/**
 * Function:GC信息bean
 *
 * @date:2016年12月2日/下午5:23:04
 * @Author:jeff.cao@aoliday.com
 * @version:1.0
 */
public class GCInfo implements Identify {

	private static final long serialVersionUID = 1L;

	private Integer id;

	/** 发送gc的机器 */
	private String host;

	/** 发送gc的进程 */
	private String proc;

	/** GC信息:Full GC等 */
	private String info;

	/** 发送GC的时间 */
	private long time;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getProc() {
		return proc;
	}

	public void setProc(String proc) {
		this.proc = proc;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public long getTime() {
		return time;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int getId() {
		return id;
	}

}
