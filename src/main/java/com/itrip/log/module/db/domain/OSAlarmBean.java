/* 
 * 创建日期 2016-02-19
 *
 * 成都澳乐科技有限公司
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 * 版权所有
 */
package com.itrip.log.module.db.domain;

/**
 * 
 * author:jeff.cao@aoliday.com date:2016年2月25日
 */
public class OSAlarmBean{

	private float load;
	private float diskUsed;
	private float memUsed;
	private String emailGroup;

	
	public String getEmailGroup() {
		return emailGroup;
	}

	public void setEmailGroup(String emailGroup) {
		this.emailGroup = emailGroup;
	}

	public float getDiskUsed() {
		return diskUsed;
	}

	public void setDiskUsed(float diskUsed) {
		this.diskUsed = diskUsed;
	}

	public float getMemUsed() {
		return memUsed;
	}

	public void setMemUsed(float memUsed) {
		this.memUsed = memUsed;
	}

	public float getLoad() {
		return load;
	}

	public void setLoad(float load) {
		this.load = load;
	}

}
