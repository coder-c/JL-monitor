/* 
 * 创建日期 2016-02-19
 *
 * 成都澳乐科技有限公司
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 * 版权所有
 */
package com.itrip.log.module.db.domain;

/**
 * 
 * author:jeff.cao@aoliday.com date:2016年3月4日
 */
public class TimeCostAlarm {

	private int id;

	private String proc;

	private String url;

	private long timeCost;

	private String emailGroup;

	private int period;

	private int requestCount;

	public String getProc() {
		return proc;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public int getRequestCount() {
		return requestCount;
	}

	public void setRequestCount(int requestCount) {
		this.requestCount = requestCount;
	}

	public void setProc(String proc) {
		this.proc = proc;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url.trim();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getTimeCost() {
		return timeCost;
	}

	public void setTimeCost(long timeCost) {
		this.timeCost = timeCost;
	}

	public String getEmailGroup() {
		return emailGroup;
	}

	public void setEmailGroup(String emailGroup) {
		this.emailGroup = emailGroup.trim();
	}

}
