package com.itrip.log.module.db.server;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.itrip.log.module.db.DaoTemplate;
import com.itrip.log.module.db.domain.EmailRecord;

/**
 * Function:XXX TODO add desc
 *
 * @date:2016年5月30日/下午6:12:28
 * @Author:jeff@aoliday.cao
 * @version:1.0
 */
public class EmailRecordManager extends DaoTemplate {


	@Override
	@SuppressWarnings("unchecked")
	public <T> List<T> getListByMap(String namespace, Map<String, Object> param) {
		List<EmailRecord> listByMap = super.getListByMap(namespace, param);
		for (EmailRecord bean : listByMap) {
			// id == count
			bean.setBugsrate((bean.getId() / (float) bean.getPv()));
		}
		return (List<T>) listByMap;
	}

	public List<EmailRecord> getManagerData(long start, long end) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("start", start);
		param.put("end", end);
		List<EmailRecord> listByMap = selectList("selectGroupByManager", param);
		return listByMap;
	}

	public List<EmailRecord> getProjectData(long start, long end) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("start", start);
		param.put("end", end);
		List<EmailRecord> listByMap = selectList("selectGroupByProject", param);
		for (EmailRecord bean : listByMap) {
			bean.setBugsrate((bean.getId() / (float) bean.getPv()));
		}
		return listByMap;
	}

	public List<EmailRecord> getPersonData(long start, long end) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("start", start);
		param.put("end", end);
		List<EmailRecord> listByMap = selectList("selectGroupByPerson", param);
		return listByMap;
	}

	@Override
	public String processClassName() {
		return EmailRecord.class.getSimpleName();
	}

}
