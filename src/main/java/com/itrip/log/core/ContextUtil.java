package com.itrip.log.core;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Function:上下文工具类
 *
 * @date:2016年8月2日/下午8:27:00
 * @Author:jeff@aoliday.cao
 * @version:1.0
 */
public class ContextUtil  implements ApplicationContextAware{

	private static ApplicationContext ctx;
	
	public static ApplicationContext getCtx() {
		return ctx;
	}

	public static void setCtx(ApplicationContext ctx) {
		ContextUtil.ctx = ctx;
	}

	@SuppressWarnings("unchecked")
	public static <T> T get(String beanId) {
		return (T) ctx.getBean(beanId);
	}

	public static <T> T get(Class<T> cls) {
		return (T) ctx.getBean(cls);
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		ContextUtil.ctx = arg0;
	}
}
