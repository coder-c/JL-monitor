package com.itrip.log.core;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Function:根服务,容器启动后加载该类,该类加载各个模块,模块都在spring-core.xml配置
 *
 * @date:2016年10月19日/下午6:36:33
 * @Author:coder_czp@126.com
 * @version:1.0
 */
public class BootstrapServer {

	private static final Logger LOG = LoggerFactory.getLogger(BootstrapServer.class);

	private List<IModule> modules = new CopyOnWriteArrayList<IModule>();

	public List<IModule> getModules() {
		return modules;
	}

	public void setModules(List<IModule> modules) {
		this.modules = modules;
	}

	public void addModule(IModule module) {
		modules.add(module);
	}

	/**
	 * 获取指定的模块
	 * 
	 * @param name
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends IModule> T getModule(String name) {
		for (IModule item : modules) {
			if (item.name().equals(name))
				return (T) item;
		}
		LOG.error("no module named:{}", name);
		return null;
	}

	/**
	 * 获取指定的模块
	 * 
	 * @param name
	 * @return
	 */
	public <T extends IModule> T getModule(Class<T> cls) {
		for (IModule item : modules) {
			if (cls.isInstance(item))
				return cls.cast(item);
		}
		LOG.error("no module for class:{}", cls);
		return null;
	}

	/***
	 * 获取模块管理的服务
	 * 
	 * @param moduleCls
	 * @param serCls
	 * @return
	 */
	public <T extends IServer> T getModuleServer(Class<? extends IModule> moduleCls, Class<T> serCls) {
		IModule module = getModule(moduleCls);
		return module.getServer(serCls);
	}

	public void start() {
		for (IModule item : modules) {
			try {
				LOG.debug("start to start module:{}", item.name());
				item.start(this);
				LOG.debug("success to start module:{}", item.name());
			} catch (Exception e) {
				LOG.error("fail to start module:" + item.name(), e);
			}
		}
	}

	public void stop() {
		for (IModule item : modules) {
			try {
				LOG.debug("start to stop module:{}", item.name());
				item.stop(this);
				LOG.debug("success to stop module:{}", item.name());
			} catch (Exception e) {
				LOG.error("fail to stop module:" + item.name(), e);
			}
		}
	}
}
