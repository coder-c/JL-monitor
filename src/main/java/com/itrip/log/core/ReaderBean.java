/* 
 * 创建日期 2016-02-19
 *
 * 成都澳乐科技有限公司
 * 电话：028-85253121 
 * 传真：028-85253121
 * 邮编：610041 
 * 地址：成都市武侯区航空路6号丰德国际C3
 * 版权所有
 */
package com.itrip.log.core;


/**
 * 
 * author:jeff.cao@aoliday.com date:2016年3月9日
 */
public class ReaderBean {

	private String file;
	private String host;
	private String name;
	private String proc;
	private String line;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
		String[] fs = file.trim().split("/+");
		this.name = fs[fs.length - 1];
		this.proc = getProc(file);
	}

	public static String getProc(String file) {
		String[] fs = file.trim().split("/+");
		int index = file.startsWith("/data") ? 2 : 1;
		for (int i = 0; i < fs.length; i++) {
			if (fs[i].equals("work")) {
				return fs[i + index];
			}
		}
		return file;
	}
	
	public String getProc() {
		return proc;
	}

	public String getName() {
		return name;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	@Override
	public String toString() {
		return "ReaderBean [file=" + file + ", host=" + host + ", name=" + name + ", line=" + line + ", proc=" + proc
				+ "]";
	}

}
