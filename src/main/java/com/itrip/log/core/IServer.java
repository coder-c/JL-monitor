package com.itrip.log.core;
/**
 * Function:所有服务的抽象
 *
 * @date:2016年10月20日/下午2:52:34
 * @Author:coder_czp@126.com
 * @version:1.0
 */
public interface IServer {
	
	void stop();
	
	void start();
	
	void setBoostServer(BootstrapServer server);
}
