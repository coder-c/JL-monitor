package com.itrip.log.core;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Function:服务的抽象接口
 *
 * @date:2016年10月20日/下午2:56:11
 * @Author:coder_czp@126.com
 * @version:1.0
 */
public abstract class AbstractModule<T extends IServer> implements IModule {

	/***
	 * 每个模块包含的服务
	 */
	protected List<T> servers = new CopyOnWriteArrayList<T>();

	@Override
	public <E extends IServer> E getServer(Class<E> cls) {
		for (T item : servers) {
			if (cls.isInstance(item))
				return cls.cast(item);
		}
		throw new RuntimeException(String.format("[%s] not add to:[%s]", cls, this));
	}

	public List<T> getServers() {
		return servers;
	}

	public void setServers(List<T> servers) {
		this.servers = servers;
	}

	public void addServer(T server) {
		servers.add(server);
	}

	@Override
	public void start(BootstrapServer root) {
		for (T t : servers) {
			t.setBoostServer(root);
		}
	}

	@Override
	public void stop(BootstrapServer root) {

	}

}
