package com.itrip.log.core;

/**
 * Function:抽象模块
 *
 * @date:2016年10月19日/下午6:40:35
 * @Author:coder_czp@126.com
 * @version:1.0
 */
public interface IModule {

	/***
	 * 模块名称
	 * 
	 * @return
	 */
	String name();

	/***
	 * 根据类获取模块里的服务
	 * @param cls
	 * @return
	 */
	<T extends IServer> T getServer(Class<T> cls);

	/***
	 * 启动模块
	 */
	void start(BootstrapServer root);

	/****
	 * 停止模块
	 */
	void stop(BootstrapServer root);
}
