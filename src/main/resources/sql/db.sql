create database if not exists monitor;

use monitor;

create table if not exists pv
(
   id                 int NOT NULL AUTO_INCREMENT,
   project            varchar(256),
   pv              	  bigint,
   create_time        bigint,
   PRIMARY KEY (id),
   INDEX index_ctime ( `create_time` )
);

create table if not exists spec_log
(
   id                 int NOT NULL AUTO_INCREMENT,
   project            varchar(256),
   host            	  varchar(256),
   user_id            int,
   log                 text(65533),
   create_time        bigint,
   PRIMARY KEY (id),
   INDEX index_ctime ( `create_time` )
);


create table if not exists email_group
(
   id                 int NOT NULL AUTO_INCREMENT,
   manager            varchar(256),
   name               varchar(256),
   email              varchar(256),
   group_name         varchar(1024),
   PRIMARY KEY (id)
);

create table if  not exists email_record
(
   id                 int NOT NULL AUTO_INCREMENT,
   project            varchar(256),
   manager            varchar(256),
   keyword            varchar(256),
   `kvid`              varchar(100), 
   name               varchar(256) comment '接收人',
   send_time          bigint comment '发送时间',
   PRIMARY KEY (id)
)default charset utf8;

create table if  not exists keyword_alarm_rule
(
   id                   int NOT NULL AUTO_INCREMENT,
   count                int,
   keyword              varchar(256),
   exclude              varchar(256),
   file_path            varchar(512),
   email_group          varchar(512),
   PRIMARY KEY (id)
);

create table if  not exists method_alarm_rule
(
   id                   int NOT NULL AUTO_INCREMENT,
   type					varchar(256),	
   proc                 varchar(256),
   service              varchar(256),
   method               varchar(512),
   email_group          varchar(512),
   time_threshold       int,
   request_count        int,
   period				int,
   PRIMARY KEY (id)
);

create table if  not exists monitor_host
(
   id                int NOT NULL AUTO_INCREMENT,
   port              int,
   host              varchar(256),
   text              varchar(256),
   user_name         varchar(256),
   pwd          varchar(512),
   PRIMARY KEY (id)
);

create table if  not exists static_method
(
   id               int NOT NULL AUTO_INCREMENT,
   static_type       varchar(512),
   api               varchar(256),
   proc              varchar(256),
   server          varchar(512),
   method          varchar(512),
   time_cost       float,
   create_time     bigint,
   PRIMARY KEY (id)
);


CREATE TABLE if not exists `static_method_m1` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
  `calls` int(11) DEFAULT NULL,
  `static_type` varchar(512) DEFAULT NULL,
  `api` varchar(128) DEFAULT NULL,
  `proc` varchar(128) DEFAULT NULL,
  `server` varchar(128) DEFAULT NULL,
  `method` varchar(128) DEFAULT NULL,
  `max_time_cost` float(11,2) DEFAULT NULL,
  `min_time_cost` float(11,2) DEFAULT NULL,
  `avg_time_cost` float(20,2) DEFAULT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `t1` int(11) DEFAULT NULL,
  `t2` int(11) DEFAULT NULL,
  `t3` int(11) DEFAULT NULL,
  `t4` int(11) DEFAULT NULL,
  `t5` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
   INDEX index_ctime ( `create_time` )
);


CREATE TABLE if not exists `static_url_m1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calls` int(11) DEFAULT NULL,
  `static_type` varchar(20) DEFAULT NULL,
  `api` varchar(20) DEFAULT NULL,
  `proc` varchar(256) DEFAULT NULL,
  `server` varchar(100) DEFAULT NULL,
  `method` varchar(512) DEFAULT NULL,
  `max_time_cost` float(10,2) DEFAULT NULL,
  `min_time_cost` float(10,2) DEFAULT NULL,
  `avg_time_cost` float(10,2) DEFAULT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `t1` int(11) DEFAULT NULL,
  `t2` int(11) DEFAULT NULL,
  `t3` int(11) DEFAULT NULL,
  `t4` int(11) DEFAULT NULL,
  `t5` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX index_ctime ( `create_time` )
);
/**按小时汇总*/
CREATE TABLE if not exists `static_h1` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
  `calls` int(11) DEFAULT NULL,
  `static_type` varchar(512) DEFAULT NULL,
  `api` varchar(128) DEFAULT NULL,
  `proc` varchar(128) DEFAULT NULL,
  `server` varchar(128) DEFAULT NULL,
  `method` varchar(128) DEFAULT NULL,
  `max_time_cost` float(11,2) DEFAULT NULL,
  `min_time_cost` float(11,2) DEFAULT NULL,
  `avg_time_cost` float(11,2) DEFAULT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `t1` int(11) DEFAULT NULL,
  `t2` int(11) DEFAULT NULL,
  `t3` int(11) DEFAULT NULL,
  `t4` int(11) DEFAULT NULL,
  `t5` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX index_ctime ( `create_time` )
);

CREATE TABLE if not exists `statistics_os` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) DEFAULT '',
  `server` varchar(11) DEFAULT '',
  `name` varchar(11) DEFAULT '',
  `ext` varchar(11) DEFAULT '',
  `val` varchar(125) DEFAULT '',
  `create_time` bigint(20) DEFAULT 0,
   PRIMARY KEY (`id`),
   INDEX index_ctime ( `create_time` )
);

create table if not exists proc_alarm
(
   id              int NOT NULL AUTO_INCREMENT,
   host      	    varchar(256),   
   shell            varchar(256),
   email_group      varchar(256),
   return_keyword   varchar(256),
   PRIMARY KEY (id)
);

create table if not exists dictionaries
(
   id              int NOT NULL AUTO_INCREMENT,
   `type`      	    varchar(256),   
   `key`            varchar(256),
   `value`      varchar(256),
   PRIMARY KEY (id)
);

create table if not exists time_cost_alarm
(
   id              int NOT NULL AUTO_INCREMENT,
   time_cost    int,
   email_group      varchar(256),
   proc      varchar(256),
   url      varchar(256),
   period   int,
   request_count int,
   PRIMARY KEY (id)
);
create table if not exists gc_info(
   id        int NOT NULL AUTO_INCREMENT,
   host      varchar(50),
   info      varchar(100),
   proc      varchar(100),
   time      bigint,
   PRIMARY KEY (id)
);
--ALTER TABLE `static_method_m1` ADD INDEX index_ctime ( `create_time` );
--ALTER DATABASE monitor CHARACTER SET utf8 COLLATE utf8_general_ci;
--ALTER TABLE dictionaries CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
